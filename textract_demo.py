import boto3
from trp import Document

# S3 Bucket Data
s3BucketName = "textract-console-us-east-2-4ce72bc0-bc2c-4cb1-a6a3-2fc4e78b4398" # owr bucket name
documentName = "test1.png" #File name


textractmodule = boto3.client(service_name='textract',
            region_name='us-east-2'
         )

# # Read image local
# with open('test1.png', 'rb') as document:
#     img = bytearray(document.read())

# # Call Amazon Textract
# response = textractmodule.detect_document_text(
#     Document={'Bytes': img}
# )


# 1. PLAINTEXT detection from documents:
response = textractmodule.detect_document_text(
    Document={
        'S3Object': {
            'Bucket': s3BucketName,
            'Name': documentName
        }
    })
print ('------------- Print Plaintext detected text ------------------------------')
for item in response["Blocks"]:
    if item["BlockType"] == "LINE":
        print ('\033[92m'+item["Text"]+'\033[92m')


#2. FORM detection from documents:
response = textractmodule.analyze_document(
    Document={
        'S3Object': {
            'Bucket': s3BucketName,
            'Name': documentName
        }
    },
    FeatureTypes=["FORMS"])
doc = Document(response)
print ('------------- Print Form detected text ------------------------------')
for page in doc.pages:
    for field in page.form.fields:
        print("Key: {}, Value: {}".format(field.key, field.value))



#2. TABLE data detection from documents:
response = textractmodule.analyze_document(
    Document={
        'S3Object': {
            'Bucket': s3BucketName,
            'Name': documentName
        }
    },
    FeatureTypes=["TABLES"])
doc = Document(response)
print ('------------- Print Table detected text ------------------------------')
for page in doc.pages:
    for table in page.tables:
        for r, row in enumerate(table.rows):
            itemName  = ""
            for c, cell in enumerate(row.cells):
                print("Table[{}][{}] = {}".format(r, c, cell.text))